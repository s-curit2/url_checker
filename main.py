import os
from urldna import UrlDNA


class UrlChecker:
    def __init__(self, url, api_key, scan_result, client):
        self.url = 'https://example.com'
        self.api_key = os.getenv('URL_DNA_API_KEY')
        self.client = UrlDNA(api_key)

    def create_scan(self):
        """
        Create a scan for a URL
        :return:
        """
        scan_result = self.client.create_scan(self.url)
        print(f'Scan ID: {scan_result.scan.id}')
        return scan_result.scan.id

    def search(self):
        """
        Perform a search query
        :return:
        """
        query = 'domain LIKE google.com'
        search_results = self.client.search(query)
        print(search_results)
        return search_results

    def get_scan_result(self):
        """
        Get scan results, Retrieve the results of a previously initiated scan using its scan ID.
        :return:
        """
        scan_id = self.create_scan()
        scan_result = self.client.get_scan(scan_id)
        print(scan_result)
        return scan_result

    def get_available_viewports(self):
        """
        List of all available viewports (device, height, width)
        :return:
        """
        viewports_results = self.client.viewports()
        print(viewports_results)
        return viewports_results

    def get_available_user_agents(self):
        user_agents_results = self.client.user_agents()
        print(user_agents_results)
        return user_agents_results
